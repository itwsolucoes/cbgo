<div id="slide">
	<?php
	$args = array('post_type' => 'slides', 'posts_per_page' => 5);
	$loop = new WP_Query($args);
	while ($loop->have_posts()) : $loop->the_post();
	if(slt_cf_field_value( "url" ) != "") {
		$url = slt_cf_field_value( "url" );
		if (!preg_match("/^http/", $url)) {
			$url = "http://" . $url;
		} ?>
		<a href="<?php echo $url; ?>" target="_blank" rel="external">
			<?php } ?>
			<?php the_post_thumbnail('slide'); ?>
			<?php
			if(slt_cf_field_value( "url" ) != "") { ?>
		</a>
	<?php }
	endwhile;
	?>
</div>