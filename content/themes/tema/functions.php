<?php
	/**
	 * Starkers functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 *
 	 * @package 	WordPress
 	 * @subpackage 	ITW
	 */

	/* ========================================================================================================================
	
	Required external files
	
	======================================================================================================================== */

	require_once( 'external/starkers-utilities.php' );
	require_once( 'external/cpt-on-menus-editor.php' );
	require_once( 'external/customize.php' );
	require_once( 'external/detect-mobile.php' );
	$detectMobile = new Mobile_Detect();

	/* ========================================================================================================================
	
	Theme specific settings

	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
	
	======================================================================================================================== */

	add_theme_support('post-thumbnails');
	add_image_size( 'slide', 920, 360, true );
	//add_image_size( $name, $width = 0, $height = 0, $crop = false );
	
	register_nav_menus(array('primary' => 'Primary Navigation'));


    // Noticias Excerpt
    add_image_size('excerpt-thumbnail', 280, 180, true);
    function custom_read_more($more){
        return '<a class="btn-read-more" href="'. get_permalink( get_the_ID() ) . '"><strong>Clique Aqui</strong> e Leia Mais</a>';
    }
    add_filter( 'excerpt_more', 'custom_read_more' );

/* ========================================================================================================================

Actions and Filters

======================================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'style_enqueuer' );
	add_action( 'wp_enqueue_scripts', 'script_enqueuer');

	// Custom login page
	add_action( 'login_enqueue_scripts', 'login_enqueue_scripts' );
	add_filter( 'login_headerurl', 'login_logo_url' );
	add_filter( 'login_headertitle', 'login_logo_url_title' );
	
	if ( function_exists( 'slt_cf_register_box') )
		add_action( 'init', 'register_my_custom_fields' );

	// Remove unnescessary meta-data
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

	// Disable HTML in comments
	add_filter( 'pre_comment_content', 'esc_html' );

	add_filter('admin_footer_text', 'remove_footer_admin');

	// Disable update warning
	if ( !current_user_can( 'edit_users' ) ) {
		add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
		add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
	}

	// Customize Dashboard widgets
	add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
	add_action('wp_dashboard_setup', 'dashboard_widgets_itw');

	// Change WordPress email sender
	add_filter( 'wp_mail_from', 'new_mail_from' );
	add_filter( 'wp_mail_from_name', 'new_mail_from_name' );

	add_filter( 'body_class', 'add_slug_to_body_class' );

	/* ========================================================================================================================
	
	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
	
	======================================================================================================================== */


	//require_once( 'custom-post-types/custom-post-type.php' );
	require_once( 'custom-post-types/slides.php' );

	/* ========================================================================================================================
	
	Custom Fields
	
	======================================================================================================================== */

	function register_my_custom_fields() {
		slt_cf_register_box( 
			array(
				'type'      => 'post',
				'title'     => 'Informações',
				'id'        => 'info-box',
				'context'   => 'normal',
				'priority'  => 'high',
				'fields'    => array(
				// array(
				// 	'name'          => 'nome',
				// 	'label'         => 'Nome',
				// 	'type'          => 'text',
				// 	'scope'         => array( 'post' ),
				// 	'capabilities'  => array( 'edit_posts' )
				// 	),
				// array(
				// 	'name'          => 'uf',
				// 	'label'         => 'Estado',
				// 	'type'          => 'select',
				// 	'options'       => array(	'AC' => 'Acre',
				// 		'AL' => 'Alagoas',
				// 		'AP' => 'Amapá',
				// 		'AM' => 'Amazonas',
				// 		'BA' => 'Bahia',
				// 		'CE' => 'Ceará',
				// 		'DF' => 'Distrito Federal',
				// 		'GO' => 'Goiás',
				// 		'ES' => 'Espírito Santo',
				// 		'MA' => 'Maranhão',
				// 		'MT' => 'Mato Grosso',
				// 		'MS' => 'Mato Grosso do Sul',
				// 		'MG' => 'Minas Gerais',
				// 		'PA' => 'Pará',
				// 		'PB' => 'Paraiba',
				// 		'PR' => 'Paraná',
				// 		'PE' => 'Pernambuco',
				// 		'PI' => 'Piauí',
				// 		'RJ' => 'Rio de Janeiro',
				// 		'RN' => 'Rio Grande do Norte',
				// 		'RS' => 'Rio Grande do Sul',
				// 		'RO' => 'Rondônia',
				// 		'RR' => 'Rorâima',
				// 		'SP' => 'São Paulo',
				// 		'SC' => 'Santa Catarina',
				// 		'SE' => 'Sergipe',
				// 		'TO' => 'Tocantins'),
				// 	'scope'         => array( 'post' ),
				// 	'capabilities'  => array( 'edit_posts' )
				// 	),
					array(
						'name'          => 'url',
						'label'         => 'URL',
						'type'          => 'text',
						'scope'         => array( 'slides' ),
						'capabilities'  => array( 'edit_posts' )
						)
					)
)
);
}

	/* ========================================================================================================================
	
	Scripts
	
	======================================================================================================================== */

	/**
	 * Add styles via wp_head()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function style_enqueuer() {
		global $detectMobile;
		
		wp_register_style( 'reset', get_template_directory_uri().'/css/reset.css', '', '', 'screen' );
		wp_enqueue_style( 'reset' );

		if ($detectMobile->isTablet()) {
			wp_register_style( 'screen', get_template_directory_uri().'/css/tablet.css', '', '', 'screen' );
			wp_enqueue_style( 'screen' );
		} elseif ($detectMobile->isMobile()){
			wp_register_style( 'screen', get_template_directory_uri().'/css/mobile.css', '', '', 'screen' );
			wp_enqueue_style( 'screen' );
		} else {
			wp_register_style( '960_16_col', get_template_directory_uri().'/css/960_16_col.css', '', '', 'screen' );
			wp_enqueue_style( '960_16_col' );
			wp_register_style( 'screen', get_template_directory_uri().'/style.css', '', '', 'screen' );
			wp_enqueue_style( 'screen' );
		}
	}	


	/**
	 * Add scripts via wp_footer()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function script_enqueuer() {
		wp_register_script( 'cycle', get_template_directory_uri().'/js/jquery.cycle.all.min.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'cycle' );
		wp_register_script( 'site', get_template_directory_uri().'/js/site.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'site' );
	}	

	/* ========================================================================================================================
	
	Comments
	
	======================================================================================================================== */

	/**
	 * Custom callback for outputting comments 
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>	
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php echo get_avatar( $comment ); ?>
				<h4><?php comment_author_link() ?></h4>
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				<?php comment_text() ?>
			</article>
			<?php endif;
		}