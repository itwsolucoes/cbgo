<?php
	/* ========================================================================================================================
	
	Custom Login Page
	
	======================================================================================================================== */

	function login_enqueue_scripts(){
		echo '
		<div class="background-cover"></div>
		<style type="text/css" media="screen">
		.background-cover
		{
			background:url('.get_bloginfo('template_directory').'/images/itw/bg.jpg) #ededed !important; 
			-webkit-background-size: cover; 
			-moz-background-size: cover; 
			-o-background-size: cover; 
			background-size: cover; 
			position:fixed; 
			top:0; 
			left:0; 
			z-index:10; 
			overflow: hidden; 
			width: 100%; 
			height:100%;
		} 
				#login
		{
			z-index:9999; 
			position:relative;
		}
		.login form
		{
			background: #00A69C;
		}
		.login h1 a
		{
			background:url('.get_bloginfo('template_directory').'/images/itw/logoITW.png) no-repeat center top !important; 
			height:85px;
		} 
		.login label{color: #fff}
		input.button-primary, button.button-primary, a.button-primary
		{
			-moz-border-radius: 3px !important;
			-webkit-border-radius: 3px !important;
			border-radius: 3px !important; 
			-khtml-border-radius: 3px !important;
			border:none !important;
			font-weight:normal !important;
			text-shadow:none !important;
		}
		.button:active, .submit input:active, .button-secondary:active
		{
			text-shadow: none !important;

		}
		.login #nav a, .login #backtoblog a
		{
			color:#686868 !important;
			text-shadow: none !important;
		}
		.login #nav a:hover, .login #backtoblog a:hover
		{
			color:#00A69C !important;
			text-shadow: none !important;
		}
		.login #nav, .login #backtoblog
		{
			font-size: 14px;
			text-align: center;
		}
		</style>
		';
	}

	function login_logo_url() {
		return 'http://itwsolucoes.com.br';
	}

	function login_logo_url_title() {
		return 'Desenvolvido por ITW Soluções';
	}

	/* ========================================================================================================================
	
	Change dashboard footer text
	
	======================================================================================================================== */

	function remove_footer_admin () {
		echo "<a href='http://itwsolucoes.com.br' target='_blank'>ITW Soluções para a web</a>";
	} 


	/* ========================================================================================================================
	
	Customize dashboard widgets
	
	======================================================================================================================== */
	function remove_dashboard_widgets() {
		global $wp_meta_boxes;
		// Main column:
		remove_meta_box('dashboard_browser_nag','dashboard','normal');
		remove_meta_box('dashboard_right_now','dashboard', 'normal');
		remove_meta_box('dashboard_recent_comments','dashboard','normal');
		remove_meta_box('dashboard_incoming_links','dashboard','normal');
		remove_meta_box('dashboard_plugins','dashboard','normal');

		// Side Column:
		remove_meta_box('dashboard_quick_press','dashboard','side');
		remove_meta_box('dashboard_recent_drafts','dashboard','side');
		remove_meta_box('dashboard_primary','dashboard','side');
		remove_meta_box('dashboard_secondary','dashboard','side');

	}

	// http://codex.wordpress.org/Dashboard_Widgets_API#Advanced:_Forcing_your_widget_to_the_top
	function dashboard_widgets_itw() {
		wp_add_dashboard_widget('custom_itw_widget', 'Suporte ITW Soluções', 'custom_dashboard_itw');
		global $wp_meta_boxes;
		$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
		$example_widget_backup = array('custom_itw_widget' => $normal_dashboard['custom_itw_widget']);
		unset($normal_dashboard['custom_itw_widget']);
		$sorted_dashboard = array_merge($example_widget_backup, $normal_dashboard);
		$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
	} 
	
	function custom_dashboard_itw() {
		echo '<h2>Bem vindo ao painel de gerenciamento do seu site!</h2><br>
		<p>Precisa de ajuda?<br>
		Entre em contato conosco através do e-mail 
		<strong><a href="mailto:suporte@itwsolucoes.com.br">suporte@itwsolucoes.com.br</a></strong>,<br>
		pelo telefone <strong>(62) 3255 - 5680</strong>,<br>
		ou consulte o <strong><a href='. get_bloginfo('template_directory'). '/manual.pdf>manual de gerenciamento.</a></strong></p><br>
		<a href="http://itwsolucoes.com.br" target="_blank"><img src=' . get_bloginfo('template_directory').'/images/itw/logoITW.png></a>
		<p><a href="http://itwsolucoes.com.br">http://itwsolucoes.com.br</a><br>
		Av. 85, nº 51, Ed. Colibri, sala 202<br>Setor Bela Vista, 74823-420<br>Goiânia - GO</p>';
	}


	function new_mail_from( $email ) {
		$email = 'suporte@itwsolucoes.com.br';

		return $email;
	}

	function new_mail_from_name( $name ) {
		$name = 'ITW Soluções';

		return $name;
	}