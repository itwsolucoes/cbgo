<?php
/**
 * The template for displaying Author Archive pages
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	ITW
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/html-header', 'parts/header' ) ); ?>

<?php if ( have_posts() ): the_post(); ?>

<h2>Autor: <?php echo get_the_author() ; ?></h2>

<?php if ( get_the_author_meta( 'description' ) ) : ?>
<?php echo get_avatar( get_the_author_meta( 'user_email' ) ); ?>
<h3>Sobre <?php echo get_the_author() ; ?></h3>
<?php the_author_meta( 'description' ); ?>
<?php endif; ?>

<?php rewind_posts(); while ( have_posts() ) : the_post(); ?>
	<article>
		<h2><a href="<?php esc_url( the_permalink() ); ?>" title="Link para <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> 
		<?php comments_popup_link('Deixe um comentário', '1 Comentário', '% Comentários'); ?>
		<?php the_content(); ?>
	</article>
<?php endwhile; ?>

<?php else: ?>
<h2>Nenhum artigo de: <?php echo get_the_author() ; ?></h2>	
<?php endif; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/footer','parts/html-footer' ) ); ?>