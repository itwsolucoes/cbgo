<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file 
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	ITW
 */

$detectMobile = new Mobile_Detect();
if ($detectMobile->isTablet()) {
	get_template_part( 'tablet/index' );
} elseif ($detectMobile->isMobile()) {
	get_template_part( 'mobile/index' );
} else {
	Starkers_Utilities::get_template_parts( array( 'parts/html-header', 'parts/header' ) ); ?>

	<?php if ( have_posts() ): ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<div class="artigo">
		<div class="titulo">
			<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			<span><?php echo the_author_meta('first_name'); ?> <?php echo the_author_meta('last_name'); ?></span>
			<div class="data"><?php the_time('d/m/Y'); ?></div>
		</div>
		<div>
			<?php the_post_thumbnail(); ?>
			<?php the_excerpt(); ?>
		</div>
	</div>
<?php endwhile;
wp_pagenavi(); ?>
<?php else: ?>
	<h2>Nenhum artigo adicionado</h2>
<?php endif; ?>

<?php get_template_part( 'parts/sidebar' ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/footer','parts/html-footer') );
}
?>