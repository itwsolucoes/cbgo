<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	ITW
 */

$detectMobile = new Mobile_Detect();
if ($detectMobile->isTablet()) {
    get_template_part( 'tablet/front-page' );
} elseif ($detectMobile->isMobile()) {
    get_template_part( 'mobile/front-page' );
} else {
    Starkers_Utilities::get_template_parts( array( 'parts/html-header', 'parts/header' ) ); ?>


    <!--Slides-->

    <div class="container_16 home-slide" xmlns="http://www.w3.org/1999/html">
        <div class="grid_16">
            <span id="prev"></span>
            <span id="next"></span>
            <?php get_template_part("loops/loop-slide") ?>
        </div>
    </div>

    <!--Conteudo-->
    <div class="container_16">
        <!--Posts-->
        <div class="grid_10">
            <article class="noticias">

                <h2 class="noticias">Fique por dentro das notícias</h2>

                <!--The Loop-->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <div class="post">

                        <!--Cabeçalho -->
                        <div class="post-header">
                            <h3>
                                <a href="<?php the_permalink() ?>" rel="bookmark" title="Ver Postagem <?php the_title_attribute(); ?>">
                                    <?php the_title(); ?>
                                </a>
                                <span><?php the_time('d/m/Y') ?></span>
                            </h3>
                            <p>Autor: <?php the_author_posts_link() ?></p>

                        </div>
                        <!---->

                        <!--Conteudo do post-->
                        <div class="entry">
                            <!--Imagem Thumbsnail-->
                            <div class="excerpt-thumb">
                                <?php the_post_thumbnail('excerpt-thumbnail', 'class=pull-left'); ?>
                            </div>
                            <!---->

                            <!--Texto do Post-->
                            <div class="post-article">
                                <?php the_excerpt(); ?>
                            </div>
                            <!----->
                        </div>
                        <!---->
                    </div> <!-- Fim Post -->

                <?php endwhile; else: ?>
                    <p>Desculpe, a consulta não encontrou nenhum resultado.</p>
                <?php endif; ?>

            </article> <!--Fim Noticias-->
        </div> <!--Fim grid_10-->

        <!--Sidebar-->
        <div class="grid_6">

            <!--Videos-->
            <div class="videos">
                <h2 class="videos">Vídeos</h2>
                <?php echo do_shortcode("[youtubefeeder]") ?>
                <a class="btn-veja-mais" href="#"><strong>Clique Aqui</strong> e Leia Mais</a>
            </div>
            <!---->


            <!-- Central de Orações-->
            <div class="central-oracao">
                <h2 class="central">Central de Orações</h2>
                <img src="<?php bloginfo('template_directory'); ?>/images/central-img.png">
                <a class="btn-veja-mais" href="#"><strong>Clique Aqui</strong> e Leia Mais</a>
            </div>
            <!---->


            <!--Conteudo Restrito-->
            <div class="acesso">
                <h2 class="acesso-restrito">Acesso Restrito</h2>
                <form>
                    <label> Usuário: <input type="text" /></label>
                    <br />
                    <label> Senha: <input type="password"></label>
                    <button type="submit" value="">Entrar</button>
                </form>
                <br clear="all">
            </div>
            <!----->

        </div> <!--Fim grid_6-->
    </div> <!--Fim container_16--.

    <!--Links Utéis-->
    <div class="container_16">
        <div class="grid_16">
            <h2 class="links-uteis">Links Utéis</h2>
        </div>

        <ul class="links-list">
            <li class="grid_4"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/links-uteis-img1.png"/></a></li>
            <li class="grid_4"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/links-uteis-img2.png"/></a></li>
            <li class="grid_4"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/links-uteis-img3.png"/></a></li>
            <li class="grid_4"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/links-uteis-img4.png"/></a></li>
            <br clear="all">
        </ul>

    </div>
    <!---->

    <?php Starkers_Utilities::get_template_parts( array( 'parts/footer','parts/html-footer') );
}
?>

