<div class="footer-links">
    <!--Menu Rodapé-->
    <div class="container_16">
        <div class="grid_16">
            <!--Chamada Menu-->
        </div>
    </div>
</div>

<footer>
    <div class="container_16">
        <div class="grid_16">
            <div class="map-frame">
                <!--Código Maps Aqui-->
            </div>
        </div>

        <!--informações e Links-->
        <div class="grid_8">
            <p class="client-info"> <strong>Covenção Batista Goiana </strong>
                <br />
                Rua 230 nº168, Setor Universitário.<br />
                Goiânia Goiás. 74605-110. <br />
                comunicacaocbg@yahoo.com.br <br />
                (062) 3092 4904 // (062) 8485 8586
            </p>

            <ul class="social-media">
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>
        </div>

        <!--Logo-->
        <div class="grid_8">
            <img class="logo-footer" src="<?php bloginfo('template_directory'); ?>/images/logo-cbgo-baixo.png">
        </div>
    </div> <!--Fim container_16-->
</footer>

<div class="copyright">
    <div class="container_16">
        <div class="grid_14">
            <p>2013 - <strong>Batistas de Goiás.</strong> Todos os direitos reservados.</p>
        </div>
        <div class="grid_2">
            <img class="logo-itw" src="<?php bloginfo('template_directory'); ?>/images/logo-itw.png">
        </div>
    </div>
</div>