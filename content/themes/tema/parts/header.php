<header>
    <!--Links e Busca-->
    <div class="container_16 top-section">
            <div class="grid_4 pull-left">
                <ul class="social-media">
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                </ul>
            </div>
        <?php get_search_form(); ?>
        </div>
</header>

<!--Logo e Mensagem-->
<div class="container_16 sub-header">
    <div class="grid_16">
         <div class="logo">
            <img src="<?php bloginfo('template_directory'); ?>/images/logo-cbgo.png">
         </div>

        <div class="random-header-message">
            <p>
                Maravilho-me de que tão depressa passásseis daquele que vos chamou à graça <br />
                de Cristo para outro evangelho; O qual não é outro, mas há alguns que vos <br />
                inquietam e querem transtornar o evangelho de Cristo.<strong> Gálatas 1:6,7</strong>
            </p>
        </div>
    </div>
 </div>

<!--Menu de Navegação-->
<div class="container_16">
    <div class="grid_16">
    <?php wp_nav_menu( array('menu' => 'cabecalho' )); ?>
    </div>
</div>


