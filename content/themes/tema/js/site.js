
jQuery(document).ready(function($) {

	$('#slide').after('<div id="nav">').cycle({
		fx: 'fade',
        prev:    '#prev',
        next:    '#next'
	});


	$('input').each(function() {

		var default_value = this.value;

		$(this).focus(function(){
			if(this.value == default_value) {
				this.value = '';
			}
		});

		$(this).blur(function(){
			if(this.value == '') {
				this.value = default_value;
			}
		});

	});

});

