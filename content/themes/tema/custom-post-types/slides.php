<?php
add_action('init', 'create_post_type_slides');

function create_post_type_slides() {
	register_post_type('slides',
		array(
			'labels' => array(
				'name' => __('Slides'),
				'singular_name' => __('Slide'),
				'add_new' => __('Adicionar novo'),
				'add_new_item' => __('Adicionar novo slide')
				),
			'public' => true,
			'has_archive' => true,
			'menu_position' => 5,
			'supports' => array('title', 'thumbnail')
			)
		);
}


function my_rewrite_flush_slides() {
	create_post_type_slides();
	flush_rewrite_rules();
}

register_activation_hook(__FILE__, 'my_rewrite_flush_slides');
?>