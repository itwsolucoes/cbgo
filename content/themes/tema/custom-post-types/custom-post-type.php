<?php 
add_action('init', 'create_post_type_produtos');

	function create_post_type_produtos() {
		register_post_type('produtos',
			array(
				'labels' => array(
					'name' => __('Produtos'),
					'singular_name' => __('Produto'),
					'add_new' => __('Adicionar novo'),
					'add_new_item' => __('Adicionar novo produto')
					),
				'public' => true,
				'has_archive' => true,
				'menu_position' => 5,
				'supports' => array('title', 'editor', 'excerpt', 'thumbnail')
				)
			);
	}

	register_taxonomy(
		"categoria",
		"produtos",
		array(
			'labels' => array(
				'name' => __('Categorias'),
				'singular_name' => __('Categoria'),
				'add_new' => __('Adicionar novo'),
				'add_new_item' => __('Adicionar novo categoria')
				),
			"rewrite" => true,
			"hierarchical" => true
			)
		);

	function my_rewrite_flush_produtos() {
		create_post_type_produtos();
		flush_rewrite_rules();
	}

	register_activation_hook(__FILE__, 'my_rewrite_flush_produtos');
	?>